#!/bin/bash

yum -y update
yum -y install haproxy
cp /vagrant/files/haproxy.cfg /etc/haproxy/haproxy.cfg
systemctl enable haproxy
systemctl start haproxy

yum -y install firewalld
systemctl enable firewalld
firewall-cmd --zone=public --add-port=80/tcp
firewall-cmd --zone=public --add-port=22/tcp

ifconfig | grep inet | grep -v 127.0.0.1 | grep -v inet6 | grep -v 10.0.2.15 # Don't use (grep -v) 127.0.0.1 or inet6 or 10.2.15
