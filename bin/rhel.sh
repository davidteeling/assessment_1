#!/bin/bash

yum -y update
yum -y install httpd

chown root /var/www/html/index.html # Change ownership of files
chgrp root /var/www/html/index.html # Change group

systemctl start httpd # Starts Apache
systemctl enable httpd # Make sure it starts when system does

ifconfig | grep inet | grep -v 127.0.0.1 | grep -v inet6 | grep -v 10.0.2.15 # Don't use (grep -v) 127.0.0.1 or inet6 or 10.2.15

echo     "#!/bin/bash
    case \$1 in
    'start')
    echo -e \"<html>\nHello there\n<br> My IP address is $(ifconfig | grep 192 | awk '{print $2}'| sed 's/^.*://')\n<br> David\n<br> CentOS 7.2 LTS\n<br> Apache HTTPD 2.4\n<br> Intel(R) Core(TM) i3-1000NG4 CPU @ 1.10GHz\n<br> </html>\">/var/www/html/index.html
       ;;

    *)
        echo \"Not a valid argument\"
        ;;
esac" > rhelIP

chmod 777 /var/www/html/index.html
chmod 777 ./rhelIP
./rhelIP start
